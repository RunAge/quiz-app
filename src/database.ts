import neDB = require('nedb-promises');

const store = neDB.create({ filename: './store.db' });

interface Question {
  question: string;
  answers: string[];
  correct: string;
}

function addQuestion(question: Question | Question[]): Promise<object | string> {
  return store.insert(question);
}

function getQuestion(find: object = {}): Promise<any> {
  return store.find(find);
}

function removeQuestion(question: Question | object): Promise<any> {
  return store.remove(question, {});
}

function removeAllQuestion(): Promise<any> {
  return store.remove({}, { multi: true });
}

function updateQuestion(question: Question | object, udpate: Question): Promise<any> {
  return store.update(question, udpate, {});
}

export {
  addQuestion,
  getQuestion,
  removeQuestion,
  removeAllQuestion,
  updateQuestion,
};
