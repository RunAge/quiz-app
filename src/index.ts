import * as bodyParser from 'body-parser';
import { createServer } from 'http';
import { Api } from './routes';

import express = require('express');

const app: express.Application = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static('./static'));

app.use('/api', Api);

const server = createServer(app);
server.listen(7331, (): void => {
  console.log('Server started http://localhost:7331'); // eslint-disable-line no-console
});
