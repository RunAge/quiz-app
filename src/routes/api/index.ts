import * as db from '../../database';
import { internalServerError } from './utils';

import express = require('express');
const router = express.Router();

router.route('/question')
  .get(async (req, res): Promise<void> => {
    try {
      res.json({
        code: 200,
        questions: await db.getQuestion(),
      });
    } catch (error) {
      console.debug(error);
      res.json(internalServerError);
    }
  })
  .post(async (req, res): Promise<void> => {
    try {
      if (!req.body.question) throw new Error('Missing req.body');
      const quest = req.body.question;
      db.addQuestion(quest);
      res.json({
        code: 200,
        message: 'Accepted.',
      });
    } catch (error) {
      console.debug(error);
      res.json(internalServerError);
    }
  })
  .delete(async (req, res): Promise<void> => {
    try {
      db.removeAllQuestion();
      res.json({
        code: 200,
        message: 'Accepted.',
      });
    } catch (error) {
      console.debug(error);
      res.json(internalServerError);
    }
  });

router.route('/question/:id')
  .get(async (req, res): Promise<void> => {
    try {
      res.json({
        code: 200,
        question: await db.getQuestion({ _id: req.params.id }),
      });
    } catch (error) {
      console.debug(error);
      res.json(internalServerError);
    }
  })
  .delete(async (req, res): Promise<void> => {
    try {
      db.removeQuestion({ _id: req.params.id });
      res.json({
        code: 200,
        message: 'Accepted.',
      });
    } catch (error) {
      console.debug(error);
      res.json(internalServerError);
    }
  })
  .put(async (req, res): Promise<void> => {
    try {
      if (!req.body.question) throw new Error('Missing req.body');
      const quest = req.body.question;
      await db.updateQuestion({ _id: req.params.id }, quest);
      res.json({
        code: 200,
        question: await db.getQuestion({ _id: req.params.id }),
      });
    } catch (error) {
      console.debug(error);
      res.json(internalServerError);
    }
  });
export { router as Api };
