const internalServerError = {
  code: 501,
  message: 'Internal server error.',
};

export {
  internalServerError,
};
