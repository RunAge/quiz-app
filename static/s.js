const qEl = document.getElementById('questions');
async function loadQuestions() {
  const data = await fetch(`${location.href}api/question`);
  const json = await data.json();
  json.questions.forEach((quest, i) => {
    const $quest = document.createElement('div');
    $quest.className = 'row py-1';
    $quest.innerHTML = `
      <h4 class="pl-2">${i + 1}. ${quest.question}</h4>
      <div class="container">
    `;
    quest.answers.forEach((answer, i) => {
      $quest.innerHTML += `
      <div class="container row pl-5">
        <input class="form-check-input" type="radio" name="${quest._id}" id="${quest._id}-${i}" value="${answer}">
        <label class="form-check-label" for="${quest._id}-${i}">${answer}</lable>
      </div>
    `;
    });
    $quest.innerHTML += `
      </div>
    `;
    qEl.appendChild($quest);
  });
}

loadQuestions();
